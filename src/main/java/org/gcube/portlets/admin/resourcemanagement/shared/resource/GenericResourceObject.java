package org.gcube.portlets.admin.resourcemanagement.shared.resource;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * <Resource>
	    <ID>8673d9d9-22ce-4731-8145-c2683943daed</ID>
	    <Type>GenericResource</Type>
	    <SubType>FHN-nodes</SubType>
	    <Scopes>/gcube/devNext</Scopes>
	    <Name>http:$$cloud.recas.ba.infn.it:8787$occi$compute$0805c2db-ae17-4418-9ecd-8e81552791d2@dded8184-c033-48af-8633-b1adcf3e3308</Name>
	    <Description>Node deployed to Provider dded8184-c033-48af-8633-b1adcf3e3308with serviceProfile3b6633b7-593a-4626-af19-61295fe23bc1</Description>
	</Resource>
 * @author pieve mail:alessandro.pieve@isti.cnr.it
 *
 */
@XmlRootElement(name = "Resource")
public class GenericResourceObject {

	private  String id;
	private  String type;
	private  String subType;
	private  String scopes;
	private  String name;
	private  String description;
	
	
	public GenericResourceObject(){}

	public GenericResourceObject(String id, String type, String subType,
			String name, String description,String scopes) {
		super();
		this.id = id;
		this.type = type;
		this.subType = subType;
		this.name = name;
		this.scopes = scopes;
		this.description = description;
	}
	
	@XmlElement(name = "ID")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@XmlElement(name = "Type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@XmlElement(name = "SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	@XmlElement(name = "Scopes")
	public String getScopes() {
		return scopes;
	}
	public void setScopes(String scopes) {
		this.scopes = scopes;
	}

	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name = "Description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
	
	
}
