package org.gcube.portlets.admin.resourcemanagement.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.portlets.admin.resourcemanagement.shared.resource.CacheList;


public class ServletResource extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8877769357760713039L;
	public void init() throws ServletException
	{
		// Do required initialization
	
	}
 
	public void doGet(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException	{
		String paramName = "resId";
		String paramValue = request.getParameter(paramName);
 		// Set response content type
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
 
 
		String message=CacheList.resourceid.get(paramValue).getBody();
		//TODO manage if not exist or null
		
		
		PrintWriter out = response.getWriter();
		out.println(message);
	}
 
	public void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException	{
		
	}
 
	public void destroy()
	{
		// do nothing.
	}
}