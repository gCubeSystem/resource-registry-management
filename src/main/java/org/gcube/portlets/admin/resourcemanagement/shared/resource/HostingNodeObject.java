package org.gcube.portlets.admin.resourcemanagement.shared.resource;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
<Resource>
<ID>a7597dbc-28a3-4ad5-ba6d-19e7a0a2d41d</ID>
<Type>GHN</Type>
<SubType>upv.es</SubType>
<Status>certified</Status>
<Name>niebla.i3m.upv.es:80</Name>
<Uptime>17:07</Uptime>
<LastUpdate>2017-03-28T10:10:33+02:00</LastUpdate>
<LocalAvailableSpace>2832216</LocalAvailableSpace>
<VirtualSize>17957</VirtualSize>
<VirtualAvailable>17676</VirtualAvailable>
<LoadLast1Min>0.87</LoadLast1Min>
<LoadLast5Min>0.36</LoadLast5Min>
<LoadLast15Min>0.39</LoadLast15Min>
<gcf-version>2.1.0-4.2.0-139932</gcf-version>
<ghn-version>2.0.0-4.2.0-132475</ghn-version>
<Scopes>/gcube/devNext;/gcube</Scopes>
</Resource>
*@author pieve mail:alessandro.pieve@isti.cnr.it
*/
@XmlRootElement(name = "Resource")
public class HostingNodeObject {
	
	private  String id;
	private  String type;
	private  String subType;
	private  String status;
	private  String name;
	private  String upTime;
	private  String lastUpdate;
	private  long localAvailableSpace;
	private  long virtualSize;
	private  long virtualAvailable;
	private  String loadLast1Min;
	private  String loadLast5Min;
	private  String loadLast15Min;
	private  String gcfVersion;
	private  String ghnVersion;
	private  String scopes;
	
	public HostingNodeObject(){}
	
	
	public HostingNodeObject(String id, String type, String subType,
			String status, String name, String upTime, String lastUpdate,
			long localAvailableSpace, long virtualSize,
			long virtualAvailable, String loadLast1Min, String loadLast5Min,
			String loadLast15Min, String gcfVersion, String ghnVersion,
			String scopes) {
		super();
		this.id = id;
		this.type = type;
		this.subType = subType;
		this.status = status;
		this.name = name;
		this.upTime = upTime;
		this.lastUpdate = lastUpdate;
		this.localAvailableSpace = localAvailableSpace;
		this.virtualSize = virtualSize;
		this.virtualAvailable = virtualAvailable;
		this.loadLast1Min = loadLast1Min;
		this.loadLast5Min = loadLast5Min;
		this.loadLast15Min = loadLast15Min;
		this.gcfVersion = gcfVersion;
		this.ghnVersion = ghnVersion;
		this.scopes = scopes;
	}

	@XmlElement(name = "ID")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name = "Type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name = "SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}

	@XmlElement(name = "Status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name = "Name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "Uptime")
	public String getUpTime() {
		return upTime;
	}
	public void setUpTime(String upTime) {
		this.upTime = upTime;
	}

	@XmlElement(name = "LastUpdate")
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@XmlElement(name = "LocalAvailableSpace")
	public long getLocalAvailableSpace() {
		return localAvailableSpace;
	}
	public void setLocalAvailableSpace(long localAvailableSpace) {
		this.localAvailableSpace = localAvailableSpace;
	}

	@XmlElement(name = "VirtualSize")
	public long getVirtualSize() {
		return virtualSize;
	}
	public void setVirtualSize(long virtualSize) {
		this.virtualSize = virtualSize;
	}

	@XmlElement(name = "VirtualAvailable")
	public long getVirtualAvailable() {
		return virtualAvailable;
	}
	public void setVirtualAvailable(long virtualAvailable) {
		this.virtualAvailable = virtualAvailable;
	}
	
	@XmlElement(name = "LoadLast1Min")
	public String getLoadLast1Min() {
		return loadLast1Min;
	}
	public void setLoadLast1Min(String loadLast1Min) {
		this.loadLast1Min = loadLast1Min;
	}

	@XmlElement(name = "LoadLast5Min")
	public String getLoadLast5Min() {
		return loadLast5Min;
	}
	public void setLoadLast5Min(String loadLast5Min) {
		this.loadLast5Min = loadLast5Min;
	}
	
	@XmlElement(name = "LoadLast15Min")
	public String getLoadLast15Min() {
		return loadLast15Min;
	}
	public void setLoadLast15Min(String loadLast15Min) {
		this.loadLast15Min = loadLast15Min;
	}

	@XmlElement(name = "gcf-version")
	public String getGcfVersion() {
		return gcfVersion;
	}
	public void setGcfVersion(String gcfVersion) {
		this.gcfVersion = gcfVersion;
	}

	@XmlElement(name = "ghn-version")
	public String getGhnVersion() {
		return ghnVersion;
	}
	public void setGhnVersion(String ghnVersion) {
		this.ghnVersion = ghnVersion;
	}


	@XmlElement(name = "Scopes")
	public String getScopes() {
		return scopes;
	}
	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
	
}
