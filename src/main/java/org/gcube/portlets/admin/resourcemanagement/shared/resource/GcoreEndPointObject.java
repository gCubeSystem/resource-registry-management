package org.gcube.portlets.admin.resourcemanagement.shared.resource;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <Resource>
	    <ID>73dfb6e1-965f-467a-8796-ef310e82eb71</ID>
	    <Type>RunningInstance</Type>
	    <SubType>VREManagement</SubType>
	    <Scopes>/gcube/devNext;/gcube/devsec;/gcube</Scopes>
	    <ServiceClass>VREManagement</ServiceClass>
	    <ServiceName>WhnManager</ServiceName>
	    <Version>2.0.0-SNAPSHOT</Version>
	    <Status>ready</Status>
	    <ghn-name>node11.d.d4science.research-infrastructures.eu:80</ghn-name>
	</Resource>
 * @author pieve
 *
 */
@XmlRootElement(name = "Resource")
public class GcoreEndPointObject {

	private  String id;
	private  String type;
	private  String subType;
	private  String scopes;
	private  String serviceClass;
	private  String serviceName;
	private  String version;
	private  String status;
	private  String ghnName;
	public GcoreEndPointObject(){}
	
	public GcoreEndPointObject(String id, String type, String subType,
			String scopes, String serviceClass, String serviceName,
			String version, String status, String ghnName) {
		super();
		this.id = id;
		this.type = type;
		this.subType = subType;
		this.scopes = scopes;
		this.serviceClass = serviceClass;
		this.serviceName = serviceName;
		this.version = version;
		this.status = status;
		this.ghnName = ghnName;
	}
	@XmlElement(name = "ID")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@XmlElement(name = "Type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@XmlElement(name = "SubType")
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	@XmlElement(name = "Scopes")
	public String getScopes() {
		return scopes;
	}
	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
	
	@XmlElement(name = "ServiceClass")
	public String getServiceClass() {
		return serviceClass;
	}
	public void setServiceClass(String serviceClass) {
		this.serviceClass = serviceClass;
	}
		
	@XmlElement(name = "ServiceName")
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	@XmlElement(name = "Version")
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	@XmlElement(name = "Status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@XmlElement(name = "ghn-name")
	public String getGhnName() {
		return ghnName;
	}
	public void setGhnName(String ghnName) {
		this.ghnName = ghnName;
	}
	
	
	
	
}
